<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
route::get('/register', 'AuthController@register');
route::post('/send', 'AuthController@send');

route::get('/master', function(){
    return view('layout.master');
});
route::get('/data-table', 'IndexController@table');

// CRUD cast
// create
Route::get('/cast/create', 'castController@create'); //mengarah ke form tambah data pemain film baru
Route::post('/cast', 'castController@store'); //menyimpan data baru ke tabel cast

// Read
Route::get('/cast', 'castController@index'); //ambil data ditampilkan ke blade/database
Route::get('/cast/{cast_id}', 'castController@show'); // route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'castController@edit'); //route untuk mengarahkan ke form edit
Route::put('/cast/{cast_id}', 'castController@update');

// Delete
Route::delete('/cast/{cast_id}', 'castController@destroy'); //route untuk delete berdasarkan id