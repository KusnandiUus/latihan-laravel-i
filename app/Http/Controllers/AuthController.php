<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function register(){
       return view('regis');
   }
   public function send(Request $request)
   {
    
    $namadepan = $request['Nama_Depan'];
    $namabelakang = $request['Nama_Belakang'];

    return view('jump', compact("namadepan", "namabelakang"));
   }

}
